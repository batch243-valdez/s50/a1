
import './App.css';
import AppNavbar from './components/App.Navbar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import PageNotFound from './pages/PageNotFound';
import CourseView from './pages/CourseView';

import { BrowserRouter, Routes, Route, Navigate} from 'react-router-dom'
import { useContext } from 'react';

import { UserContext } from './context/UserContext';

// import { UserProvider } from './context/UserContext'

function App() {
  
  // const [user, setUser] = useState(localStorage.getItem("email"))

  // const unSetUser = () => {
  //   localStorage.clear()
  //   setUser("")
  // }

  const { user } = useContext(UserContext)

  console.log(user)
  return (
    // <UserProvider value={{user, setUser, unSetUser}}>
      <BrowserRouter>
        <AppNavbar />
          <Routes>
            <Route
              path="*"
              element={<PageNotFound />}
              />
            <Route 
              path="/"
              element={<Home />}
            />
            <Route 
              path="/courses"
              element={<Courses />}
            />
            <Route 
              path="/courses/:courseId"
              element={<CourseView />}
            />
            <Route 
              path="/register"
              element={!user ? <Register /> : <Navigate to="/"/>}
            />
            <Route 
              path="/login"
              element={!user ? <Login /> : <Navigate to="/"/>}
            />
          </Routes>
      </BrowserRouter>
    // </UserProvider>
  );
}

export default App;
