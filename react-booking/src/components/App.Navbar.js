// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';

import { Container, Nav, Navbar } from 'react-bootstrap';

import { useContext } from 'react';

import { NavLink } from 'react-router-dom'

import { UserContext } from '../context/UserContext';

export default function AppNavbar(){
  
  const { user, unSetUser  } = useContext(UserContext)

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={NavLink} to="/">
            Course Booking
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="ml-auto" id="basic-navbar-nav">
          <Nav className={user? "offset-md-8 col-md-4 d-flex justify-content-around align-items-center" : "offset-md-10 col-md-2 d-flex justify-content-around align-items-center"}>
            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
            {!user && 
            <>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </>
            }
            {user && 
            <>
            <button onClick={unSetUser}><Nav.Link as={NavLink} to="/login">Log out</Nav.Link></button>
            </>
            }
          </Nav> 
        </Navbar.Collapse>
      </Container>
    </Navbar>
  ) 

}