import Button from "react-bootstrap/Button";
import { Link } from 'react-router-dom'
// bootstrap Grid system
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col"

const Banner = () => {
  return (
    <Row>
      <Col className="text-center mt-5 mb-5">
          <h1>Zuitt Coding Bootcamp</h1>
          <p>Opportunities for everyone, everywhere.</p>
          <Button as={Link} to="/courses" variant = "success">Enroll Now!</Button>
      </Col>
    </Row>
  )
}

export default Banner