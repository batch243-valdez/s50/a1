import { useState, useEffect } from 'react';
import { Button, Card, Col} from 'react-bootstrap';
import { useContext } from 'react';
import { UserContext } from '../context/UserContext';
import { Link } from 'react-router-dom'

const CourseCard = ({course}) => {
  // console.log(course);

  const { _id, name, description, price, slots} = course;

  const { user } = useContext(UserContext)

  const [enrollees, setEnrollees] = useState(0);
  const [slotsAvailable, setSlotsAvailable] = useState(slots)
  const [error, setError] = useState(null)
  const [isAvailable, setIsAvailable] = useState(true)

  useEffect(() => {
    if(slotsAvailable === 0){
      setIsAvailable(false);
      alert("Congrats");
      return
    }
  }, [slotsAvailable])

  function handleClick() {

    if(slotsAvailable === 0){
      setError("No more slots available")
      return
    }
    setEnrollees(enrollees+1);
    setSlotsAvailable(slotsAvailable-1)

  }

  return (
    <>
      <Col className='mt-3' xs={12} md = {4}>
      
        <Card className='cardHighlight'>
          <Card.Body className='d-flex flex-column'>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className='mt-3'>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle><h6>Price</h6></Card.Subtitle>
            <Card.Text><span>&#8369;</span>{price}</Card.Text>
            <Card.Subtitle className='mb-3'>Enrollees: {enrollees}</Card.Subtitle>
            <Card.Subtitle className='mb-2'>Slots available: {slotsAvailable}</Card.Subtitle>
            {user ?
            <Button as = {Link} to={`/courses/${_id}`} disabled={!isAvailable} onClick={handleClick} className='mt-auto' variant={error ? "secondary" : "primary"}>Details</Button>
            :
            <Button as={Link} to="/login" disabled={!isAvailable} onClick={handleClick} className='mt-auto' variant={error ? "secondary" : "primary"}>Enroll</Button>
            }
          </Card.Body>
          {error && <div className="error">{error}</div>}
        </Card>
      </Col>
    </>
  )

}

export default CourseCard