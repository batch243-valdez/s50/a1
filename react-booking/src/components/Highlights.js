//import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
	
	return(
		<Row className = "mt-3 mb-3">
		
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Learn From Home</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, ipsam nesciunt, soluta accusantium voluptate architecto vero adipisci inventore ut laboriosam veniam! At officia asperiores excepturi dolores quasi pariatur ea repudiandae?
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Study Now, Pay Later</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi beatae animi nobis, dolores nihil repellat, perferendis harum exercitationem praesentium nesciunt enim quia eius distinctio alias assumenda voluptatibus vel quaerat? Impedit.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				        	<h2>Be part of our community</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint vero nisi pariatur inventore nam voluptate iure explicabo libero rerum. Dolorum dolorem iste reprehenderit temporibus quas ducimus consequatur saepe sapiente vero?
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		)
}