import { createContext } from "react"
import { useState, useEffect } from "react";
import Swal from 'sweetalert2'


  export const UserContext = createContext();
  
  export const UserProvider = ({ children }) => {
    const [user, setUser] = useState(null)

  const unSetUser = () => {
    localStorage.removeItem("user")
    setUser(null)
    Swal.fire({
      title: "Welcome back to the outside world!",
      icon: "info",
      text: "Hanggang sa muli! Paalam!"
    })
  }

  useEffect(() => {

    const token = localStorage.getItem("user")
    
    if(token){
      setUser(token)
    }

    const retrieveUserDetails = async (token) => {
      const response = await fetch(`http://localhost:4000/users/profile`, {
        headers: {
          authorization: `Bearer ${token}`
        }
      })
      
      const json = await response.json()
      console.log(json)
      setUser({id: json._id, isAdmin: json.isAdmin})
    }
    
    retrieveUserDetails(token)
  
  }, [])


  return (
    <UserContext.Provider value={{user, setUser, unSetUser}}>
      { children }
    </UserContext.Provider>
  )
}
