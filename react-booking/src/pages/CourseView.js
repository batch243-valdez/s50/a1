import { useEffect, useState } from "react"
import { Container, Card, Button, Row, Col } from 'react-bootstrap'
import { useParams, useNavigate } from "react-router-dom"
import  Swal  from 'sweetalert2'

export default function CourseView(){

  const [name, setName] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState(0)

  const { history } = useNavigate()
  const { courseId } = useParams()
  
  useEffect(() => {
    fetch(`http://localhost:4000/courses/${courseId}`)
    .then(res => res.json())
    .then(data => {
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
    })
  }, [courseId])
  
  const token = localStorage.getItem("user")
  
  const enroll = (courseId) => {
    fetch(`http://localhost:4000/users/enroll/${courseId}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data === true){
        Swal.fire({
          title: "Successfully enrolled!",
          icon: "success",
          text:"congrats!"
        })
        history("/courses")
      }else{
        Swal.fire({
          title: "Error",
          icon: "error",
          text: "something went wrong!"
        })
        history("/")
      }
    })
  }

  return (
    <Container>
      <Row>
        <Col lg = {{span: 6, offset: 3}}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Class Schedule</Card.Subtitle>
              <Card.Text>8 am - 5 pm</Card.Text>
              <Button variant="primary" className="w-100" onClick={() => enroll(courseId)}>Enroll</Button>
            </Card.Body>
					</Card>
        </Col>
      </Row>
    </Container>
  )

}