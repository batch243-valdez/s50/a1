import CourseCard from '../components/CourseCard'
import { Container, Row } from 'react-bootstrap'
import { useEffect, useState } from 'react'

const Courses = () => {

  const [courses, setCourses] = useState([])
  
  useEffect(() => {
    fetch(`http://localhost:4000/courses/allActiveCourses`)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setCourses(data.map(course => {
        return (
          <CourseCard key={course._id} course={course}/>
        )
      }))
    })
  }, []);

  return (
    <Container>
      <Row>
        { courses }
      </Row>
    </Container>
  )

}

export default Courses