
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Row, Col, Container } from 'react-bootstrap'

import { useEffect, useState, useContext} from 'react';

import Swal from 'sweetalert2';

import { UserContext } from '../context/UserContext';

export default function Login() {

  const [email, setEmail] = useState('');
  const [password, setPassword1] = useState('');
  const [error, setError] = useState(null)

  const { setUser } = useContext(UserContext)


  useEffect(() => {
    // if(!email || !password)
    //   return setError('Please fill all the fields')
    setError(null)

   }, [email, password])

  const handleSubmit = (e) => {
    e.preventDefault();
    
    // localStorage.setItem("email", email)
    // setUser(localStorage.getItem("email"))
    const fetchLogin = async () => {
      
      const response = await fetch(`http://localhost:4000/users/login`, {
        method: "POST",
        headers: { 'Content-Type' : 'application/json' },
        body : JSON.stringify({ email, password })
      })

      const json = await response.json()
        
        if(!response.ok){
          Swal.fire({
            title: "Authentication failed!",
            icon: "error",
            text: `${json.error}`
          })
          setError(json.error)
        }
        
        if(response.ok){
          localStorage.setItem("user", JSON.stringify(json.accessToken))
          retrieveUserDetails(json.accessToken)
          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to our website!"
          })
        }
      }

      const retrieveUserDetails = async (token) => {
        const response = await fetch(`http://localhost:4000/users/profile`, {
          headers: {
            authorization: `Bearer ${token}`
          }
        })
        
        const json = await response.json()
        console.log(json)
        setUser({id: json._id, isAdmin: json.isAdmin})
    }

    fetchLogin()
  }

  return (
    <Container>
      <Row>
        <Col className='col-md-4 offset-md-4 col-8 offset-2'>
          <Form className='mt-5 bg-light p-3'>
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email" 
                placeholder="Enter email" 
                required 
                onChange={(e) => setEmail(e.target.value)}
                value={email}
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                required 
                onChange={(e) => setPassword1(e.target.value)}
                value={password}
              />
            </Form.Group>

            <Button className='w-100' onClick={handleSubmit} disabled={error} variant="primary" type="submit">
              Submit
            </Button>
            {error && <div className='error mt-3'>{error}</div>}
          </Form>
        </Col>
      </Row>
    </Container>
  )
}