import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Row, Col, Container } from 'react-bootstrap'
import { useContext } from 'react';
import { UserContext } from '../context/UserContext';
import { useEffect, useState } from 'react';

import Swal from "sweetalert2"

export default function Register(){

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');
  const [error, setError] = useState(null)
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNo, setMobileNo] = useState('')

  const { setUser } = useContext(UserContext)

   useEffect(() => {
    if(!email || !password || !password2)
      return setError('Please fill all the fields')
    if(password !== password2)
      return setError("Password does not match")
    setError(null)

   }, [email, password, password2])

  const handleSubmit = (e) => {
    e.preventDefault();
    // setEmail('');
    // setPassword('');
    // setPassword2('');
    // localStorage.setItem("email", email)
    // setUser(localStorage.getItem("email"))
    // alert(`Congratulations ${email} You are now registered!`);

    const fetchRegister = async () => {
      
      const response = await fetch(`http://localhost:4000/users/register`, {
        method: "POST",
        headers: { 'Content-Type' : 'application/json' },
        body : JSON.stringify({ email, password, password2, firstName, lastName, mobileNo })
      })

      const json = await response.json()
        
        if(!response.ok){
          Swal.fire({
            title: "failed to register!",
            icon: "error",
            text: `${json.error}`
          })
          setError(json.error)
        }
        
        if(response.ok){
          localStorage.setItem("user", JSON.stringify(json.accessToken))
          setUser(json.accessToken)
          Swal.fire({
            title: "Registered Successfully!!",
            icon: "success",
            text: "Welcome to our website!"
          })
        }
      }

      fetchRegister()



  }

  return (

    <Container>
      <Row>
        <Col className='col-md-4 offset-md-4 col-8 offset-2'>
          <Form onSubmit={handleSubmit} className='mt-5 bg-light p-3'>

            <Form.Group className="mb-3" controlId="firstname">
              <Form.Label>FirstName</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="First Name" 
                required 
                onChange={(e) => setFirstName(e.target.value)}
                value={firstName}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastname">
              <Form.Label>LastName</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Last Lame" 
                required 
                onChange={(e) => setLastName(e.target.value)}
                value={lastName}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email" 
                placeholder="Enter email" 
                required 
                onChange={(e) => setEmail(e.target.value)}
                value={email}
              />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                required 
                onChange={(e) => setPassword(e.target.value)}
                value={password}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                required 
                onChange={(e) => setPassword2(e.target.value)}
                value={password2}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileno">
              <Form.Label>Mobile No</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Mobile No" 
                required 
                onChange={(e) => setMobileNo(e.target.value)}
                value={mobileNo}
              />
            </Form.Group>

            <Button className='w-100' disabled={error} variant="primary" type="submit">
              Submit
            </Button>
            {error && <div className='error mt-3'>{error}</div>}
          </Form>
        </Col>
      </Row>
    </Container>
  )
}